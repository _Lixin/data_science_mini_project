from django.conf.urls import url

from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),
    url(r'^$', views.enter, name='enter'),
    url(r'^input_url/(?P<username>(.+))/$', views.input_url, name='input_url'),
    url(r'post_enter', views.post_enter, name='post_enter'),
    url(r'^post_url/(.+)/$', views.post_url, name='post_url'),
    url(r'playlist', views.playlist, name='playlist'),
    url(r'song_list', views.SongListAll.as_view(), name='song_list'),
    url(r'^(?P<pk>[0-9]+)/$', views.SongDetail.as_view(), name='song_detail'),
    url(r'refresh_wc', views.refresh_wc, name='refresh_wc'),
    url(r'gen_lyrics', views.gen_lyrics, name='gen_lyrics'),
    url(r'refresh_lyrics', views.refresh_lyrics, name='refresh_lyrics'),
]
