from django.db import models

# Create your models here.
class Song(models.Model):
    song_name = models.CharField(max_length=80, unique=False)
    lyrics = models.TextField()
    playlist_name = models.CharField(max_length=80, unique=False)	
    artist = models.CharField(max_length=80, unique=False)

class User(models.Model):
    username = models.CharField(max_length=80, unique=True)