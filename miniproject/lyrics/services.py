import numpy as np
import pandas as pd
import os
import csv
import string
import random
import codecs
import spotipy
import PIL.ImageOps   
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from . import util
from os import path
from PIL import Image
from .models import Song
from wordcloud import WordCloud
from collections import Counter
from nltk.stem.porter import *
from sklearn.feature_extraction.text import TfidfVectorizer

STATIC_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')
DATA_PATH = os.path.join(STATIC_PATH, 'data')
CSV_PATH = os.path.join(DATA_PATH, 'users_lyrics.csv')
STOPWORDS_PATH = os.path.join(DATA_PATH, 'stop-word-list.csv')
CLEANED_WORDS_PATH = os.path.join(DATA_PATH, 'cleaned_words.txt')
IMAGE_PATH = os.path.join(STATIC_PATH, 'images')
BG_PATH = os.path.join(IMAGE_PATH, 'node.jpg')
WC_PATH = os.path.join(IMAGE_PATH, 'wordcloud.jpg')

# run once from shell
def csv_to_db():
    print("Now the path is ", CSV_PATH)
    with open(CSV_PATH, encoding="utf8") as f:
            reader = csv.reader(f)
            # skip the header row
            next(reader, None)
            # The first column is the index (pk)
            for row in reader:
                _, created = Song.objects.get_or_create(
                    playlist_name=row[1],
                    artist=row[2],
                    song_name=row[3],
                    lyrics=row[4],
                    )

def get_token(username, scope=None, client_id = None,
        client_secret = None, redirect_uri = None):       
    try:
        sp_oauth = util.get_sp_oauth(username, 
            scope = scope, 
            client_id = client_id, 
            client_secret = client_secret, 
            redirect_uri = redirect_uri)

        token_info = util.is_token_cached(sp_oauth)
        if not token_info:
            print("\nToken is not cached. Need to get redirected url.")
            get_token_result = dict({'is_token_cached': False})
            get_url_result = util.get_redirected_url(sp_oauth)
            get_token_result.update(get_url_result)
            return get_token_result
        else:
            print("\nGreat! Token is cached.")
            get_token_result = dict({'is_token_cached': True})
            get_token_result.update(token_info)
            return get_token_result            
    except Exception as e:
        pass

def get_playlist(username, token):
    song_list = []
    if token:
        sp = spotipy.Spotify(auth=token)
        playlists = sp.user_playlists(username)
        for playlist in playlists['items']:
            if playlist['owner']['id'] == username:
                list_name=playlist['name']
                results = sp.user_playlist(username, playlist['id'], fields="tracks,next")
                tracks = results['tracks']
                for i,item in enumerate(tracks['items']):
                    track=item['track']
                    l=len(track['artists'])
                    if(l<2):
                        artist=track['artists'][0]['name']
                    else:
                        artist=""
                        for i in range(l):
                            artist=artist+track['artists'][i]['name']
                            if(i<l-1):
                                artist=artist+","                           
                    song=track['name']
                    t=(list_name,artist,song)
                    song_list.append(t)
    C = np.array(song_list)    
    return C
    # df = pd.DataFrame(C)
    # df.columns=['Playlist_name','Artist','Song']
    # return df

def wrangle():
    data = []
    with open(CSV_PATH, encoding="utf8") as f:
            reader = csv.reader(f)
            # skip the header row
            next(reader, None)
            for row in reader:
                data.append(row)
    data = np.array(data)
    
    # Lowercasing, remove punctuation
    for entry in data:
        edited = entry[4] # Lyrics
        edited = "".join(l for l in edited if l not in string.punctuation)
        edited = edited.lower()
        entry[4] = edited
    
    # Remove stopwords
    with open(STOPWORDS_PATH) as f:
        stopwords = set(f.read().split(", "))
        
    for entry in data:
        text = entry[4]
        entry[4] = " ".join([word for word in text.split() if word not in stopwords])
    
    # Apply stemming
    ps = PorterStemmer()
    for entry in data:
        entry[4] = " ".join([ps.stem(word) for word in entry[4].split()])
    
    # export to txt file
    cleaned_lyrics = []
    for entry in data:
        cleaned_lyrics.append(entry[4])
    with open(CLEANED_WORDS_PATH, 'w', encoding="utf8") as f:        
        f.write("\n".join(cleaned_lyrics))
 
def genwc():
    # if os.path.exists(WC_PATH):
    #     return
    # else:
    if not os.path.exists(CLEANED_WORDS_PATH):
        wrangle()
    gen_wc_fig()
            
def gen_wc_fig():   
    # cleaned_df = pd.read_table(CLEANED_WORDS_PATH, encoding='ISO-8859-1', header=None)
    cleaned_df = pd.read_table(CLEANED_WORDS_PATH, header=None)
    cleaned_df.columns = ["cleaned_words"]
    
    # Ignore too common terms (max_df = .25)
    v = TfidfVectorizer(max_df = .25)
    X = v.fit_transform(cleaned_df["cleaned_words"].dropna())
    
    inds = (-X.mean(axis=0).A).argsort()[0]
    scores = np.squeeze(X.mean(axis=0).A)[inds]
    words = np.array(v.get_feature_names())[inds]
    
    im = Image.open(BG_PATH)
    mask = np.array(im)
    # wordcloud = WordCloud(mask=mask, background_color = "white", width=600, height=600, scale=4)
    wordcloud = WordCloud(mask=mask, background_color = "white")
    wordcloud.generate_from_frequencies(dict(zip(words, scores)))
    
    fig = plt.figure(figsize=(5, 5))
    plt.imshow(wordcloud.recolor(color_func=blue_color_func, random_state=3),
               interpolation="bilinear")
    plt.axis("off")
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
    fig.savefig(WC_PATH)
    plt.close()

def blue_color_func(word, font_size, position, orientation, random_state=None,
                    **kwargs):
    return "hsl(210, 60%%, %d%%)" % random.randint(40, 60) 