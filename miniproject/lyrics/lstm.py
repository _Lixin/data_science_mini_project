import os
import sys
import numpy as np
import pandas as pd
from os import path
os.environ['KERAS_BACKEND']='theano'
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

STATIC_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')
DATA_PATH = os.path.join(STATIC_PATH, 'data')
TRAIN_PATH = os.path.join(DATA_PATH, 'training_lyrics.csv')
HDF5_PATH = os.path.join(DATA_PATH, "checkpoints_char", "weights-improvement-23-0.7170.hdf5")
LYRICS_GEN_PATH = os.path.join(DATA_PATH, 'lyrics_gen.txt')

def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds+1e-10) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def gen_lyrics():
    print("\nstart generate lyrics...")
    training_df = pd.read_csv(TRAIN_PATH)
    training_df.info()
    song_arr = [str.lower(str(' '.join(training_df.iloc[i, -1].split()))) for i in range(0, 15)]
    whole_text = ' '.join(song_arr)

    # create mapping of unique words to integers
    chars = sorted(list(set(whole_text)))
    char_to_int = dict((c, i) for i, c in enumerate(chars))
    int_to_char = dict((i, c) for i, c in enumerate(chars))

    n_chars = len(whole_text)
    n_vocab = len(chars)
    print("Total chars: ", n_chars)
    print("Total Vocab: ", n_vocab)

    # prepare the dataset of input to output pairs encoded as integers
    seq_length = 80
    dataX = []
    dataY = []
    for song in song_arr:
        n_song_chars = len(song)
        for i in range(0, n_song_chars - seq_length, 1):
            seq_in = song[i:i + seq_length]
            seq_out = song[i + seq_length]
            dataX.append([char_to_int[w] for w in seq_in])
            dataY.append(char_to_int[seq_out])
    n_patterns = len(dataX)
    print('patterns: ', n_patterns)
    # reshape X to be [samples, time steps, features]
    y = np_utils.to_categorical(dataY)
    X = np.array([np_utils.to_categorical(char, num_classes=n_vocab) for char in [p for p in dataX]])
    # define the LSTM model
    model = Sequential()
    model.add(LSTM(256, stateful=True, input_shape=(X.shape[1], X.shape[2]), batch_input_shape=(1, X.shape[1], X.shape[2]), return_sequences=True))
    # model.add(LSTM(256, input_shape=(X.shape[1], X.shape[2]), return_sequences=True))
    model.add(Dropout(0.1))
    model.add(LSTM(128, stateful=True, input_shape=(X.shape[1], X.shape[2]), batch_input_shape=(1, X.shape[1], X.shape[2])))
    model.add(Dropout(0.1))
    model.add(Dense(y.shape[1], activation='softmax'))
    # load the network weights
    model.load_weights(HDF5_PATH)
    model.compile(loss='categorical_crossentropy', optimizer='adam')

    # pick a random seed
    start = np.random.randint(0, len(dataX)-1)
    pattern = X[start]

    # generate characters
    lyrics_gen = ""
    for i in range(1000):
        prediction = model.predict(np.reshape(pattern, (1, X.shape[1], X.shape[2])), verbose=0)[0]
        index = sample(prediction, 0.7)
        result = int_to_char[index]
        lyrics_gen+=str(result)
        sys.stdout.write(result)
        pattern = np.concatenate([pattern, np_utils.to_categorical(index, num_classes=n_vocab)], axis=0)
        pattern = pattern[1:len(pattern)]

    # export to txt file
    with open(LYRICS_GEN_PATH, 'w', encoding="utf8") as f:        
        f.write(lyrics_gen)
    print('\nDone.')

def get_lyrics_gen():
    lyrics_gen = ""
    if not os.path.exists(LYRICS_GEN_PATH):
        gen_lyrics()
    with open(LYRICS_GEN_PATH, encoding="utf8") as f:
        lyrics_gen = f.read()
    return lyrics_gen


