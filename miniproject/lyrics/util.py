# shows a user's playlists (need to be authenticated via oauth)
from __future__ import print_function
import os
import webbrowser
import spotipy
from spotipy import oauth2

def get_sp_oauth(username, scope=None, client_id=None, client_secret=None, redirect_uri=None):
    ''' get oauth of spotify
        Parameters:
        - username - the Spotify username
        - scope - the desired scope of the request
        - client_id - the client id of your app
        - client_secret - the client secret of your app
        - redirect_uri - the redirect URI of your app
    '''
    if not client_id:
        client_id = os.getenv('SPOTIPY_CLIENT_ID')

    if not client_secret:
        client_secret = os.getenv('SPOTIPY_CLIENT_SECRET')

    if not redirect_uri:
        redirect_uri = os.getenv('SPOTIPY_REDIRECT_URI')

    if not client_id:
        print('''
            You need to set your Spotify API credentials. You can do this by
            setting environment variables like so:

            export SPOTIPY_CLIENT_ID='your-spotify-client-id'
            export SPOTIPY_CLIENT_SECRET='your-spotify-client-secret'
            export SPOTIPY_REDIRECT_URI='your-app-redirect-url'

            Get your credentials at     
                https://developer.spotify.com/my-applications
        ''')
        raise spotipy.SpotifyException(550, -1, 'no credentials set')

    sp_oauth = oauth2.SpotifyOAuth(client_id, client_secret, redirect_uri,
                                   scope=scope, cache_path=".cache-" + username)
    return sp_oauth

def is_token_cached(sp_oauth):
    # try to get a valid token for this user, from the cache,
    # if not in the cache, the create a new (this will send
    # the user to a web page where they can authorize this app)
    token_info = sp_oauth.get_cached_token()
    return token_info

# if is_token_cached returns False
def get_redirected_url(sp_oauth):
    ''' returns the user token suitable for use with the spotipy.Spotify constructor
    '''
    # try to get a valid token for this user, from the cache,
    # if not in the cache, the create a new (this will send
    # the user to a web page where they can authorize this app)

    print('''
    
            User authentication requires interaction with your
            web browser. Once you enter your credentials and
            give authorization, you will be redirected to
            a url.  Paste that url you were directed to to
            complete the authorization.
    
        ''')
    auth_url = sp_oauth.get_authorize_url()

    try:
        webbrowser.open(auth_url)
        print("\nOpened %s in your browser" % auth_url, "\n")
        get_url_result = dict({'is_browser_opened': True})
        message = "Please navigate here: %s" % auth_url
        get_url_result.update({'message': message})
        # return get_url_result
    except:    
        print("\nmessage", message)
        get_url_result.update({'is_browser_opened': False})
    return get_url_result

def get_user_token(sp_oauth, response=""):
    try:
        code = sp_oauth.parse_response_code(response)
        token_info = sp_oauth.get_access_token(code)
        # Auth'ed API request
        if token_info:
            return token_info['access_token']
        return None
    except:
        print("\nignore SpotifyOauthError")
