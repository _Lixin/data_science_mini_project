from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views import generic
from django.shortcuts import redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.views.decorators.http import require_POST
try:
    from django.utils import simplejson as json
except ImportError:
    import json
from . import services as svs
from . import lstm
from . import util
from .forms import URLForm, UserForm
from .models import Song

# username = '4fj69ucfgunxa1e14zvb2zl5v' # hard code temporarily
SCOPE = 'playlist-modify-public playlist-modify-private'
CLIENT_ID = 'cb3af6ad98814f52b61d3b11044b04ce'
CLIENT_SECRET = 'b108b4373f884cc881a21cb78a587464'
REDIRECT_URI = 'http://localhost/'

def enter(request):
    form = UserForm()
    return render(request, 'lyrics/enter.html', {'form': form})

# @csrf_exempt
@csrf_protect
def post_enter(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            username = form.data['username'].strip()
            print("username:", username)
            return process_get_token(request, username)
    else:
        print("\nUser form is invalid.")

def input_url(request, username):
    return process_get_token(request, username)

# @csrf_exempt
@csrf_protect
def post_url(request, username):
    print("\ninside post_url")
    if request.method == 'POST':
        print("\nusername:", username)
        form = URLForm(request.POST)
        if form.is_valid():
            print("\n URL form is valid.")
            redirectd_url = form.data['redirected_url'].strip()
            sp_oauth = util.get_sp_oauth(username,
                                         scope=SCOPE,
                                         client_id=CLIENT_ID,
                                         client_secret=CLIENT_SECRET,
                                         redirect_uri=REDIRECT_URI)
            access_token = util.get_user_token(sp_oauth, redirectd_url)
            print("\n access token:", access_token)
        else:
            print("\n URL form is invalid \n")
        if access_token is None:
            # retry because of 1SpotifyOauthError Bad Request
            return redirect(reverse('input_url', kwargs={'username':username}))
        request.session['username'] = username
        request.session['access_token'] = access_token
        return redirect(reverse('playlist'))

# @csrf_exempt
def playlist(request):
    print("\ninside playlist")
    username = request.session.get('username', None)
    access_token = request.session.get('access_token', None)
    print("\nusername:", username)
    print("\naccess_token:", access_token)
    playlist = svs.get_playlist(username, access_token)
    if playlist is not None:
        # empty playlist is false
        if not playlist.size:
            raise Http404("No playlist found for the given user.")
        print("\nnumber of columns:", len(playlist[0]))
        playlist = playlist[:, 2] # get song name only
    plays = paginate(request, playlist)

    template_name = 'lyrics/playlist.html'
    context = {'username': username,
               'playlist': plays}
    return render(request, template_name, context)

class SongListAll(generic.ListView):
    model = Song
    template_name = "lyrics/song_list.html"
    context_object_name = 'songs'
    paginate_by = 8
    def get_context_data(self, **kwargs):
        context = super(SongListAll, self).get_context_data(**kwargs)
        song_list = Song.objects.all()
        paginator = Paginator(song_list, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            songs = paginator.page(page)
        except PageNotAnInteger:
            songs = paginator.page(1)
        except EmptyPage:
            songs = paginator.page(paginator.num_pages)

        context['songs'] = songs
        return context
    #   def get_queryset(self):
    #       return Song.objects.filter(user=self.request.user)

class SongDetail(generic.DetailView):
    model = Song
    context_object_name = 'song'
    template_name = "lyrics/song_detail.html"

@require_POST
def refresh_wc(request):
    if request.method == 'POST':
        message = "refresh_wc called"
        print("\n", message)
        svs.genwc()
    ctx = {'message': message}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')

# @require_POST
def gen_lyrics(request):
    lyrics_gen = lstm.get_lyrics_gen()
    template_name = "lyrics/gen_lyrics.html"
    ctx = {'lyrics_gen': lyrics_gen}
    return render(request, template_name, ctx)

@require_POST
def refresh_lyrics(request):
    if request.method == 'POST':
        message = "refresh_lyrics called"
        print("\n", message)
        lstm.gen_lyrics()
        lyrics_gen = lstm.get_lyrics_gen()
    ctx = {'message': message,
           'lyrics_gen': lyrics_gen}
    # use mimetype instead of content_type if django < 5
    return HttpResponse(json.dumps(ctx), content_type='application/json')

def process_get_token(request, username):
    token_info = svs.get_token(username,
                               scope=SCOPE,
                               client_id=CLIENT_ID,
                               client_secret=CLIENT_SECRET,
                               redirect_uri=REDIRECT_URI)
    print("\nToken info:", token_info)
    if token_info['is_token_cached'] is True:
        # token is cached
        access_token = token_info['access_token']
        request.session['username'] = username
        request.session['access_token'] = access_token
        return redirect(reverse('playlist'))
    else:
        template_name = 'lyrics/input_url.html'
        context = dict({'username': username})
        context.update(token_info)
        return render(request, template_name, context)

def paginate(request, content_list):
    page = request.GET.get('page')
    paginator = Paginator(content_list, 8) # Show 25 contacts per page
    try:
        contents = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        contents = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        contents = paginator.page(paginator.num_pages)
    return contents
