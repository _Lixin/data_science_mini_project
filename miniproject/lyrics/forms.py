from django import forms
from .models import User

class URLForm(forms.Form):
    redirected_url = forms.CharField(label='Redirected url', max_length=1000)

class UserForm(forms.Form):
    model = User